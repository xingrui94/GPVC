# GPVC

GPVC(Get Pixel Values from Corrdinates)是一个根据坐标，提取遥感影像像元值的桌面小工具，基于C#(.NET Framework4.7.2)开发，界面部分采用了WPF框架，以尽可能适应各种分辨率的屏幕。

- 首先，要准备好遥感影像文件，**其格式应为“tif格式”**，如下图，这是标准的Landsat遥感影像文件。

![遥感影像文件.png](./imgs/遥感影像文件.png)

- 其次，需要准备好用于提取像元值的坐标数据文件。如下图所示，**其格式必须是Excel文件**（其中的坐标为WGS84地理坐标）。

![像元值坐标点.png](./imgs/像元值坐标点.png)

- 下图是双击运行GPVC软件的界面截图：

![ScreenShot_1.png](./imgs/ScreenShot_1.png)

- 下图是使用GPVC软件，成功提取像元值坐标的截图：

![ScreenShot_2.png](./imgs/ScreenShot_2.png)

- 下图是最终提取出来的像元值数据, 这里提取的文件只有1个波段，因此，提取的结果中只有1个波段的像元值数据（这里只是测试数据，真是的像元值可能是地表反射率，值一般应介于0-1之间）。

![像元值提取结果.png](./imgs/像元值提取结果.png)
